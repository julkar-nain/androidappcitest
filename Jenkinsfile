class Constants {
    static final String DEBUG_BUILD = 'Debug'
    static final String RELEASE_BUILD = 'Release'
}

def getBuildType() {
    switch(BRANCH_NAME) {
        case 'master':
            return Constants.RELEASE_BUILD
        default:
            return Constants.DEBUG_BUILD
    }
}

def isReleaseCandidate() {
    return ("${env.BRANCH_NAME} =~/(develop|master)/")
}

pipeline {
    agent any

    environment {
        RELEASE_KEY_PASS = credentials('releaseKeyPass')
    }

    stages {
        stage('Clean') {
            steps {
                sh './gradlew clean'
            }
        }
        stage('Build') {
            steps {
            script {
                    VARIANT = getBuildType()
                    sh "./gradlew build${VARIANT} -x test -P releaseKeyPass=${RELEASE_KEY_PASS}"
                }
            }
        }
        stage('Test') {
            steps {
                script {
                    VARIANT = getBuildType()
                    sh "./gradlew test${VARIANT}UnitTest"
                }
            }
        }
        stage('Deploy APK') {
            steps {
                script {
                    VARIANT = getBuildType()
                    sh "./gradlew assemble${VARIANT} -P releaseKeyPass=${RELEASE_KEY_PASS}"
                }
            }
        }
    }

    post {
         always{
                script {
                    VARIANT = getBuildType()
                    archiveArtifacts artifacts: "**/*-${VARIANT.toLowerCase()}.apk",
                    onlyIfSuccessful: true
                }
         }
    }
}
